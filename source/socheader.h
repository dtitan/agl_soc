/*
 * socheader.h
 *
 *  Created on: 20-May-2021
 *      Author: Shubham Mishra
 */

#ifndef SOURCE_SOCHEADER_H_
#define SOURCE_SOCHEADER_H_


/*================== Includes =============================================*/
//#include "sox_cfg.h"

/*================== Macros and Definitions ===============================*/

/**
 * @ingroup CONFIG_SOX
 * the cell capacity used for SOC calculation, in this case Ah counting
 * Specified once according to data sheet of cell usually.
 * \par Type:
 * float
 * \par Unit:
 * mAh
 * \par Default:
 * 20000.0
*/
#define SOX_CELL_CAPACITY               20000.0f

/**
 * This structure contains all the variables relevant for the SOX.
 *
 */
typedef struct {
    uint8_t sensor_cc_used;  /*!< time in ms before the state machine processes the next state, e.g. in counts of 1ms    */
    float cc_scaling;        /*!< scaling for the C-C value from sensor for average value */
    float cc_scaling_min;    /*!< scaling for the C-C value from sensor for min value */
    float cc_scaling_max;    /*!< scaling for the C-C value from sensor for max value */
    uint8_t counter;                        /*!< general purpose counter */
} SOX_STATE_s;


/**
 * struct definition for 4 different values: in two current directions (charge, discharge) for two use cases (peak and continuous)
 */
typedef struct {
    float current_Charge_cont_max;  /*!< maximum current for continues charging     */
    float current_Charge_peak_max;  /*!< maximum current for peak charging          */
    float current_Discha_cont_max;  /*!< maximum current for continues discharging  */
    float current_Discha_peak_max;  /*!< maximum current for peak discharging       */
} SOX_SOF_s;

/**
 * state of charge (SOC). Since SOC is voltage dependent, three different values are used, min, max and mean
 * SOC defined as a float number between 0.0 and 100.0 (0% and 100%)
 */
typedef struct {
    float min;  /*!< minimum SOC    */
    float max;  /*!< maximum SOC    */
    float mean; /*!< mean SOC       */
    float reserved1;/*!< reserved for future use */
    float reserved2;/*!< reserved for future use */
    float reserved3;/*!< reserved for future use */
    float reserved4;/*!< reserved for future use */
} SOX_SOC_s;

/**
 * struct definition for calculating the linear SOF curve. The SOF curve is SOC,
 * voltage, temperature and charge/discharge dependent.
 */
typedef struct {
    float Slope_TLowDischa;
    float Offset_TLowDischa;
    float Slope_THighDischa;
    float Offset_THighDischa;

    float Slope_TLowCharge;
    float Offset_TLowCharge;
    float Slope_THighCharge;
    float Offset_THighCharge;

    float Slope_SocDischa;
    float Offset_SocDischa;
    float Slope_SocCharge;
    float Offset_SocCharge;

    float Slope_VoltageDischa;
    float Offset_VoltageDischa;
    float Slope_VoltageCharge;
    float Offset_VoltageCharge;
}SOF_curve_s;


/*================== Constant and Variable Definitions ====================*/


/*================== Function Prototypes ==================================*/

/**
 * @brief   initializes startup SOC-related values like lookup from nonvolatile ram at startup
 */
extern void SOC_Init(uint8_t cc_present);

/**
 * @brief   initializes the area for SOF (where derating starts and is fully active).
 *
 * Pseudocode for linear function parameter extraction with 2 points
 *  slope = (y2 - y1) / (x2-x1)
 *  offset = y1 - (slope) * x1
 *  function y= slope * x + offset
 */
extern void SOF_Init(void);

/**
 * @brief   sets SOC value with a parameter between 0.0 and 100.0.
 *
 * @param   soc_value_min   SOC min value to set
 * @param   soc_value_max   SOC maxn value to set
 * @param   soc_value_mean  SOC mean value to set
 */
extern void SOC_SetValue(float soc_value_min, float soc_value_max, float soc_value_mean);

/**
 * @brief   initializes the SOC values with lookup table (mean, min and max).
 */
extern void SOC_RecalibrateViaLookupTable(void);

/**
 * @brief   integrates current over time to calculate SOC.
 */
extern void SOC_Calculation(SOX_SOC_s*);

/**
 * @brief   triggers SOF calculation
 *
 * Calculation made with the function SOF_Calculate().
 */
extern void SOF_Calculation(void);

#endif /* SOURCE_SOCHEADER_H_ */
