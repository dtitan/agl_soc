/*
 * batterycellcfg.h
 *
 *  Created on: 23-May-2021
 *      Author: Shubham Mishra
 */

#ifndef SOURCE_BATTERYCELLCFG_H_
#define SOURCE_BATTERYCELLCFG_H_

/**
 * Define if discharge current are positive negative, default is positive
 */
#define POSITIVE_DISCHARGE_CURRENT          1



/***********************Battery state variables*****************************/
/**
 * Symbolic names for battery system state
 */
typedef enum {
    BMS_CHARGING,     /*!< battery is charged */
    BMS_DISCHARGING,  /*!< battery is discharged */
    BMS_RELAXATION,   /*!< battery relaxation ongoing */
    BMS_AT_REST,      /*!< battery is resting */
} BMS_CURRENT_FLOW_STATE_e;

#endif /* SOURCE_BATTERYCELLCFG_H_ */
