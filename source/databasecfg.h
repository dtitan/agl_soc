/*
 * databasecfg.h
 *
 *  Created on: 23-May-2021
 *      Author: HP
 */

#ifndef SOURCE_DATABASECFG_H_
#define SOURCE_DATABASECFG_H_

/**
 * @ingroup CONFIG_BATTERYSYSTEM
 * number of pack voltage inputs measured by current sensors (like IVT-MOD)
 * \par Type:
 * int
 * \par Default:
 * 3
*/
#define BS_NR_OF_VOLTAGES_FROM_CURRENT_SENSOR      3

/**
 * data block struct of current measurement
 */
typedef struct {
    /* Timestamp info needs to be at the beginning. Automatically written on DB_WriteBlock */
    uint32_t timestamp;                         /*!< timestamp of database entry                */
    uint32_t previous_timestamp;                /*!< timestamp of last database entry           */
    int32_t current;                                       /*!< unit: mA                */
    float voltage[BS_NR_OF_VOLTAGES_FROM_CURRENT_SENSOR];  /*!< unit: mV                */
    float temperature;                                     /*!< unit: 0.1&deg;C             */
    float power;                                           /*!< unit: W                */
    float current_counter;                                 /*!< unit: A.s                */
    float energy_counter;                                  /*!< unit: W.h                */
    uint8_t state_current;
    uint8_t state_voltage;
    uint8_t state_temperature;
    uint8_t state_power;
    uint8_t state_cc;
    uint8_t state_ec;
    uint8_t newCurrent;
    uint8_t newPower;
    uint32_t previous_timestamp_cur;                       /*!< timestamp of current database entry   */
    uint32_t timestamp_cur;                                /*!< timestamp of current database entry   */
    uint32_t previous_timestamp_cc;                        /*!< timestamp of C-C database entry   */
    uint32_t timestamp_cc;                                 /*!< timestamp of C-C database entry   */
} DATA_BLOCK_CURRENT_SENSOR_s;

/**
 * data block struct of sox
 */
typedef struct {
    /* Timestamp info needs to be at the beginning. Automatically written on DB_WriteBlock */
    uint32_t timestamp;                 /*!< timestamp of database entry                */
    uint32_t previous_timestamp;        /*!< timestamp of last database entry           */
    float soc_mean;                     /*!< 0.0 <= soc_mean <= 100.0           */
    float soc_min;                      /*!< 0.0 <= soc_min <= 100.0            */
    float soc_max;                      /*!< 0.0 <= soc_max <= 100.0            */
    uint8_t state;                      /*!<                                    */
} DATA_BLOCK_SOX_s;

#endif /* SOURCE_DATABASECFG_H_ */
