/*
 * socmain.c
 *
 *  Created on: 20-May-2021
 *      Author: Shubham Mishra
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "socheader.h"
#include "batterycellcfg.h"
#include "databasecfg.h"

/*SOX variables*/
static SOX_STATE_s sox_state = {
		.sensor_cc_used         = 0,
		.cc_scaling             = 0.0,
		.cc_scaling_min         = 0.0,
		.cc_scaling_max         = 0.0,
		.counter                = 0,
};

/*current measurement data*/
static DATA_BLOCK_CURRENT_SENSOR_s sox_current_tab = {0,0,0,{0.0f,0.0f,0.0f},0.0f,0.0f,0.0f,0.0f,0,0,0,0,0,0,0,0,0,0,0,0};
/*min and max data from DB*/
static DATA_BLOCK_SOX_s sox;
/*timestamp data for current*/
static uint32_t soc_previous_current_timestamp = 0;
/*for current-counter*/
static uint32_t soc_previous_current_timestamp_cc = 0;
/*File pointer*/
FILE *fp = NULL;

void getFileData()
{
	char line[14];
	fgets(line, 14, fp);
	char *tk;
	tk = strtok(line, ",");
	sox_current_tab.current = (int) (atoi(tk) * 1000);
	tk = strtok(line, "\n");
	sox_current_tab.current_counter = (float) (atof(tk) * 3600);

	sox_current_tab.timestamp = time(NULL);
	sox_current_tab.timestamp_cur = time(NULL);
	sox_current_tab.timestamp_cc = time(NULL);
}

/*to initialise the SOC init values*/
void SOC_Init(uint8_t cc_present) {
	/**/
	SOX_SOC_s soc = {50.0, 50.0, 50.0, 0, 0, 0, 0};

	/*file opr*/
	fp = fopen("Data1c.csv", "r");
	/*read and store file data*/
	getFileData();
	//DB_ReadBlock(&sox_current_tab, DATA_BLOCK_ID_CURRENT_SENSOR);
	// NVM_getSOC(&soc);

	/**/
	if (cc_present) {
		soc_previous_current_timestamp_cc = sox_current_tab.timestamp_cc;
		sox_state.sensor_cc_used = 1;


		if (POSITIVE_DISCHARGE_CURRENT) {
			sox_state.cc_scaling = soc.mean + 100.0f*sox_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
			sox_state.cc_scaling_min = soc.min + 100.0f*sox_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
			sox_state.cc_scaling_max = soc.max + 100.0f*sox_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
		} else {
			sox_state.cc_scaling = soc.mean - 100.0f*sox_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
			sox_state.cc_scaling_min = soc.min - 100.0f*sox_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
			sox_state.cc_scaling_max = soc.max - 100.0f*sox_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
		}


		sox.soc_mean = soc.mean;
		sox.soc_min = soc.min;
		sox.soc_max = soc.max;
		if (sox.soc_mean > 100.0f) { sox.soc_mean = 100.0; }
		if (sox.soc_mean < 0.0f)   { sox.soc_mean = 0.0;   }
		if (sox.soc_min > 100.0f)  { sox.soc_min = 100.0;  }
		if (sox.soc_min < 0.0f)    { sox.soc_min = 0.0;    }
		if (sox.soc_max > 100.0f)  { sox.soc_max = 100.0;  }
		if (sox.soc_max < 0.0f)    { sox.soc_max = 0.0;    }
		/* Alternatively, SOC can be initialized with {V,SOC} lookup table if available */
		/* with the function SOC_Init_Lookup_Table() */
		sox.state = 0;
		sox.timestamp = 0;
		sox.previous_timestamp = 0;
	} else {
		soc_previous_current_timestamp = sox_current_tab.timestamp_cur;
		sox_state.sensor_cc_used = 0;
	}
	//   DB_WriteBlock(&sox, DATA_BLOCK_ID_SOX);
}

int BMS_GetBatterySystemState()
{
	return BMS_DISCHARGING;
}

void SOC_Calculation(SOX_SOC_s *soc) {
	uint32_t timestamp = 0;
	uint32_t previous_timestamp = 0;

	uint32_t timestamp_cc = 0;
	uint32_t previous_timestamp_cc = 0;

	uint32_t timestep = 0;

	//DATA_BLOCK_CURRENT_SENSOR_s cans_current_tab;
	//SOX_SOC_s soc = {50.0, 50.0, 50.0, 0, 0, 0, 0};
	float deltaSOC = 0.0;

	if (BMS_GetBatterySystemState() == BMS_AT_REST) {
		/* Recalibrate SOC via LUT */
		// SOC_RecalibrateViaLookupTable();
		/*do nothing*/
	} else {
		/* Use coulomb/current counting */
		if (sox_state.sensor_cc_used == 0) {
			getFileData();
			//  DB_ReadBlock(&sox_current_tab, DATA_BLOCK_ID_CURRENT_SENSOR);

			timestamp = sox_current_tab.timestamp_cur;
			previous_timestamp = sox_current_tab.previous_timestamp_cur;
			/*update timestamps*/
			sox_current_tab.previous_timestamp_cur = sox_current_tab.timestamp_cur;
			sox_current_tab.previous_timestamp_cc = sox_current_tab.timestamp_cc;

			if (soc_previous_current_timestamp != timestamp) {  /* check if current measurement has been updated */
				timestep = timestamp - previous_timestamp;
				if (timestep > 0) {
					// NVM_getSOC(&soc);
					/* Current in charge direction negative means SOC increasing --> BAT naming, not ROB */
					/* soc_mean = soc_mean - (sox_current_tab.current *mA* /(float)SOX_CELL_CAPACITY (*mAh*)) * (float)(timestep) * (10.0/3600.0); */ /*milliseconds*/

					if (POSITIVE_DISCHARGE_CURRENT) {
						deltaSOC = (((sox_current_tab.current)*(float)(timestep)/10))/(3600.0f * SOX_CELL_CAPACITY); /* ((mA *ms *(1s/1000ms)) / (3600(s/h) *mAh)) *100% */
					} else {
						deltaSOC = -(((sox_current_tab.current)*(float)(timestep)/10))/(3600.0f * SOX_CELL_CAPACITY); /* ((mA *ms *(1s/1000ms)) / (3600(s/h) *mAh)) *100% */
					}
					soc->mean = soc->mean - deltaSOC;
					soc->min = soc->min - deltaSOC;
					soc->max = soc->max - deltaSOC;
					if (soc->mean > 100.0f) { soc->mean = 100.0; }
					if (soc->mean < 0.0f)   { soc->mean = 0.0;   }
					if (soc->min > 100.0f)  { soc->min = 100.0;  }
					if (soc->min < 0.0f)    { soc->min = 0.0;    }
					if (soc->max > 100.0f)  { soc->max = 100.0;  }
					if (soc->max < 0.0f)    { soc->max = 0.0;    }

					sox.soc_mean = soc->mean;
					sox.soc_min = soc->min;
					sox.soc_max = soc->max;

					// NVM_setSOC(&soc);

					sox.state++;
					//   DB_WriteBlock(&sox, DATA_BLOCK_ID_SOX);
				}
			} /* end check if current measurement has been updated */
			/* update the variable for the next check */
			soc_previous_current_timestamp = sox_current_tab.timestamp;

		} else {
			// DB_ReadBlock(&sox_current_tab, DATA_BLOCK_ID_CURRENT_SENSOR);
			getFileData();
			timestamp_cc = sox_current_tab.timestamp_cc;
			previous_timestamp_cc = sox_current_tab.previous_timestamp_cc;

			if (previous_timestamp_cc != timestamp_cc) {  /* check if cc measurement has been updated */
				//   DB_ReadBlock(&cans_current_tab, DATA_BLOCK_ID_CURRENT_SENSOR);

			/*	if (POSITIVE_DISCHARGE_CURRENT == 1) {
					sox.soc_mean = sox_state.cc_scaling - 100.0f*cans_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
					sox.soc_min = sox_state.cc_scaling_min - 100.0f*cans_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
					sox.soc_max = sox_state.cc_scaling_max - 100.0f*cans_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
				} else {
					sox.soc_mean = sox_state.cc_scaling + 100.0f*cans_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
					sox.soc_min = sox_state.cc_scaling_min + 100.0f*cans_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
					sox.soc_max = sox_state.cc_scaling_max + 100.0f*cans_current_tab.current_counter/(3600.0f*(SOX_CELL_CAPACITY/1000.0f));
				}*/

				soc->mean = sox.soc_mean;
				soc->min = sox.soc_min;
				soc->max = sox.soc_max;
				if (sox.soc_mean > 100.0f) { sox.soc_mean = 100.0; }
				if (sox.soc_mean < 0.0f)   { sox.soc_mean = 0.0;   }
				if (sox.soc_min > 100.0f)  { sox.soc_min = 100.0;  }
				if (sox.soc_min < 0.0f)    { sox.soc_min = 0.0;    }
				if (sox.soc_max > 100.0f)  { sox.soc_max = 100.0;  }
				if (sox.soc_max < 0.0f)    { sox.soc_max = 0.0;    }
				//  NVM_setSOC(&soc);

				sox.state++;
				//  DB_WriteBlock(&sox, DATA_BLOCK_ID_SOX);
			}
			soc_previous_current_timestamp_cc = sox_current_tab.timestamp_cc;
		}
	}
}


int main()
{
	SOX_SOC_s soc = {50.0, 50.0, 50.0, 0, 0, 0, 0};

	/*soc initialisation*/
	SOC_Init(0);	/*0 : No current sensor present*/

	/*TODO: SOC_Calculation as seperate process, remove while
	 * SOC data is also available in sox data structure*/
	while(1){
		SOC_Calculation(&soc);
		//(soc.min, soc.mean, soc.max) // (sox.min, sox.mean, sox.max)
	}

	fclose(fp);
	return 0;
}

